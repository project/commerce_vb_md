<?php

namespace Drupal\commerce_vb_md\Controller;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_vb_md\VictoriaBankBackRefHandler;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Back ref route handler.
 */
class VictoriaBankBackRefController extends ControllerBase {
  
  /**
   * Victoria Bank Handler.
   *
   * @var \Drupal\commerce_vb_md\VictoriaBankBackRefHandler
   */
  protected VictoriaBankBackRefHandler $backRefHandler;
  
  /**
   * The request object.
   *
   * @var Request
   */
  protected $request;
  
  /**
   * Constructs a new CartController object.
   *
   * @param  \Drupal\commerce_vb_md\VictoriaBankBackRefHandler  $backRefHandler
   *    Handler for backref.
   * @param  \Symfony\Component\HttpFoundation\Request  $request
   *    Current request.
   */
  public function __construct(VictoriaBankBackRefHandler $backRefHandler, Request $request) {
    $this->backRefHandler = $backRefHandler;
    $this->request = $request;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_vb_md.back_ref'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }
  
  /**
   *  Redirect user after success or failed payment from VB.
   *
   * @param  \Drupal\commerce_order\Entity\Order  $commerce_order
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function index(Order $commerce_order) {
    $selfReference = $this->request->query->get('self_reference');
    
    //If order successful payed, and click first time success, vb submit form with post data
    //user is anonymous and don't have any order payment access. just redirect again to this url.
    if ($selfReference != 1) {
      $url = Url::fromRoute('commerce_vb_md.back_ref', [
        'commerce_order' => $commerce_order->id(),
        'self_reference' => 1,
      ]);
      
    }
    else {
      $url = $this->backRefHandler->redirect($commerce_order);
    }
    return new RedirectResponse($url->toString());
  }
  
}
