<?php

namespace Drupal\commerce_vb_md\Controller;

use Drupal\commerce_vb_md\VictoriaBankManager;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VictoriaBankCallbackController extends ControllerBase {
  
  /**
   * Victoria Bank Handler
   *
   * @var \Drupal\commerce_vb_md\VictoriaBankManager
   */
  protected VictoriaBankManager $victoriaBankManager;
  
  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;
  
  /**
   * Initialize
   *
   * @param  \Drupal\commerce_vb_md\VictoriaBankManager  $victoriaBankManager
   * Vb manager
   */
  public function __construct(VictoriaBankManager $victoriaBankManager, Request $request) {
    $this->victoriaBankManager = $victoriaBankManager;
    $this->request = $request;
  }
  
  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_vb_md.manager'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }
  
  
  /**
   * Preprocess callback.
   */
  public function index() {
    try {
      $this->victoriaBankManager->preprocessResponse($this->request);
    } catch (\Exception $e) {
      $this->victoriaBankManager->getLogger()->error($e->getMessage());
    }
    return new Response('...');
  }
  
}
