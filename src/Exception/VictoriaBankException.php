<?php

namespace Drupal\commerce_vb_md\Exception;

/**
 * Exception thrown if an error which can only be found on VictoriaBank occurs.
 */
class VictoriaBankException extends \RuntimeException {

}
