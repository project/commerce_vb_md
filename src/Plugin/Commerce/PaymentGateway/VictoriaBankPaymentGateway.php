<?php

namespace Drupal\commerce_vb_md\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase;
use Drupal\commerce_vb_md\PluginForm\PaymentAuthorizationForm;
use Drupal\commerce_vb_md\VictoriaBankManager;
use Drupal\commerce_vb_md\VictoriaBankSerializer;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides the VictoriaBank payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "victoria_bank",
 *   label = "Victoria Bank",
 *   display_label = "Victoria Bank",
 *   payment_method_types = {"victoria_bank"},
 *   forms = {
 *     "add-payment" = "Drupal\commerce_payment\PluginForm\ManualPaymentAddForm",
 *     "receive-payment" = "Drupal\commerce_payment\PluginForm\PaymentReceiveForm",
 *     "offsite-payment" = "Drupal\commerce_vb_md\PluginForm\PaymentOffsiteForm",
 *   },
 *   payment_type = "victoria_bank",
 * )
 */
class VictoriaBankPaymentGateway extends PaymentGatewayBase implements VictoriaBankPaymentGatewayInterface {

  /**
   * Vb Serializer.
   *
   * @var \Drupal\commerce_vb_md\VictoriaBankSerializer
   */
  private VictoriaBankSerializer $victoriaBankSerializer;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private LanguageManagerInterface $languageManager;

  /**
   * FileSystem manager.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private FileSystemInterface $fileSystem;

  /**
   * /**
   * Constructs a new PaymentGatewayBase object.
   *
   * @param  array  $configuration
   *   A configuration array containing information about the plugin instance.
   * @param  string  $plugin_id
   *   The plugin_id for the plugin instance.
   * @param  mixed  $plugin_definition
   *   The plugin implementation definition.
   * @param  \Drupal\Core\Entity\EntityTypeManagerInterface  $entity_type_manager
   *   The entity type manager.
   * @param  \Drupal\commerce_payment\PaymentTypeManager  $payment_type_manager
   *   The payment type manager.
   * @param  \Drupal\commerce_payment\PaymentMethodTypeManager  $payment_method_type_manager
   *   The payment method type manager.
   * @param  \Drupal\Component\Datetime\TimeInterface  $time
   *   The time.
   * @param  \Drupal\commerce_vb_md\VictoriaBankSerializer  $victoriaBankSerializer
   *   Serializer vb.
   * @param  \Drupal\Core\Language\LanguageManagerInterface  $languageManager
   *   Language managers.
   * @param  \Drupal\Core\File\FileSystemInterface  $fileSystem
   *   FileSystem manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, VictoriaBankSerializer $victoriaBankSerializer, LanguageManagerInterface $languageManager, FileSystemInterface $fileSystem) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->victoriaBankSerializer = $victoriaBankSerializer;
    $this->languageManager = $languageManager;
    $this->vbTimezone = new \DateTimeZone("Europe/Bucharest");
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_vb_md.serializer'),
      $container->get('language_manager'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'instructions' => [
          'value' => '',
          'format' => 'plain_text',
        ],
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merch_name'] = [
      '#type' => 'textfield',
      "#title" => 'MERCH_NAME',
      '#description' => $this->t('Merchant name (recognizable by cardholder)'),
      '#default_value' => $this->configuration['merch_name'] ?? NULL,
      '#required' => TRUE,
    ];
    $form['merch_url'] = [
      '#type' => 'textfield',
      "#title" => 'MERCH_URL',
      '#description' => $this->t('Merchant primary web site URL in format  http://www.merchantsitename.domain'),
      '#default_value' => $this->configuration['merch_url'] ?? NULL,
      '#required' => TRUE,
    ];

    $form['merchant'] = [
      '#type' => 'number',
      "#title" => 'MERCHANT',
      '#description' => $this->t('Merchant ID assigned by bank'),
      '#default_value' => $this->configuration['merchant'] ?? NULL,
      '#required' => TRUE,
    ];

    $form['terminal'] = [
      '#type' => 'number',
      "#title" => 'TERMINAL',
      '#description' => $this->t('Merchant Terminal ID assigned by bank'),
      '#default_value' => $this->configuration['terminal'] ?? NULL,
      '#required' => TRUE,
    ];

    $form['merch_address'] = [
      '#type' => 'textfield',
      "#title" => 'MERCH_ADDRESS',
      '#description' => $this->t('Merchant company registered office address'),
      '#default_value' => $this->configuration['merch_address'] ?? NULL,
      '#required' => TRUE,
    ];
    $form['support_contacts'] = [
      '#type' => 'textfield',
      "#title" => $this->t('Support Contacts'),
      '#description' => $this->t('Support contacts information, telephones'),
      '#default_value' => $this->configuration['support_contacts'] ?? NULL,
      '#required' => TRUE,
    ];
    $form['return_policy_uri'] = [
      '#type' => 'textfield',
      "#title" => $this->t('Return/refund policy URI'),
      '#description' => $this->t('Return/refund policy URI link, example: /node/1'),
      '#default_value' => $this->configuration['return_policy_uri'] ?? NULL,
      '#required' => TRUE,
    ];
    $form['auto_complete_sales'] = [
      '#type' => 'checkbox',
      "#title" => $this->t('Auto complete sales'),
      '#description' => $this->t('Automatic submit complete sales'),
      '#default_value' => $this->configuration['auto_complete_sales'] ?? NULL,
      '#required' => FALSE,
    ];

    if (!$this->existVictoriaPubPem()) {
      $form['victoria_pub_pem'] = [
        '#markup' => "<p>victoria_pub.pem not found in private folder: " . VictoriaBankManager::PRIVATE_VICTORIA_PUB_PEM . "</p>",
      ];
    }

    if (!$this->existsKeyPem()) {
      $form['key_pem'] = [
        '#markup' => "<p>key.pem not found in private folder: " . VictoriaBankManager::PRIVATE_KEY_PEM . "</p>",
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$this->existVictoriaPubPem()) {
      $form_state->setErrorByName('victoria_pub_pem', $this->t("victoria_pub.pem not found in private folder:") . VictoriaBankManager::PRIVATE_VICTORIA_PUB_PEM);
    }
    if (!$this->existsKeyPem()) {
      $form_state->setErrorByName('key_pem', $this->t("key_pem not found in private folder:") . VictoriaBankManager::PRIVATE_KEY_PEM);
    }
    $values = $form_state->getValue($form['#parents']);
    $refundUri = $values['return_policy_uri'];
    //Check if enter valid url format.
    try {
      Url::fromUserInput($refundUri)->toString();
    } catch (\Exception $e){
      $form_state->setErrorByName('return_policy_uri', $this->t("Invalid Return/refund policy URI"));
    }
  }


  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merch_name'] = $values['merch_name'];
      $this->configuration['merch_url'] = $values['merch_url'];
      $this->configuration['merchant'] = $values['merchant'];
      $this->configuration['terminal'] = $values['terminal'];
      $this->configuration['merch_address'] = $values['merch_address'];
      $this->configuration['support_contacts'] = $values['support_contacts'];
      $this->configuration['return_policy_uri'] = $values['return_policy_uri'];
      $this->configuration['auto_complete_sales'] = $values['auto_complete_sales'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentInstructions(PaymentInterface $payment) {
    $instructions = [];
    if (!empty($this->configuration['instructions']['value'])) {
      $instructions['instruction_payment'] = [
        '#type' => 'processed_text',
        '#text' => $this->configuration['instructions']['value'],
        '#format' => $this->configuration['instructions']['format'],
      ];

    }
    return $instructions;
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $received = FALSE) {
    $this->assertPaymentState($payment, ['new']);
    $payment->state = $received ? 'completed' : 'pending';
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['pending']);
    $payment->state = 'voided';
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function authorizationPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['new', 'pending']);
    $payment->state = 'authorization';
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function completePayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    $payment->state = 'completed';
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization', 'completed']);
    $payment->state = 'refund';
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $payment_state = $payment->getState()->value;
    $operations = [];
    $operations['authorization'] = [
      'title' => $this->t('Authorization'),
      'page_title' => $this->t('Authorization'),
      'plugin_form' => 'authorization-payment',
      'access' => $payment_state == 'pending',
    ];
    $operations['complete'] = [
      'title' => $this->t('Complete'),
      'page_title' => $this->t('Complete Payment'),
      'plugin_form' => 'complete-payment',
      'access' => $payment_state == 'authorization',
    ];
    $operations['refund'] = [
      'title' => $this->t('Refund'),
      'page_title' => $this->t('Refund Payment'),
      'plugin_form' => 'refund-payment',
      'access' => in_array($payment_state, ['completed', 'authorization']),
    ];
    $operations['void'] = [
      'title' => $this->t('Void'),
      'page_title' => $this->t('Void payment'),
      'plugin_form' => 'void-payment',
      'access' => $payment_state == 'pending',
    ];
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultForms() {

    $defaultForms = parent::getDefaultForms();
    $defaultForms['authorization-payment'] = PaymentAuthorizationForm::class;
    $defaultForms['complete-payment'] = 'Drupal\commerce_vb_md\PluginForm\PaymentCompleteForm';
    $defaultForms['refund-payment'] = 'Drupal\commerce_vb_md\PluginForm\PaymentRefundForm';

    return $defaultForms;
  }

  /**
   * {@inheritdoc}
   */
  public function getNotifyUrl() {
    throw new AccessDeniedHttpException();
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    //Order checkout flow is changed in callback of system, because we don't
    // have any information before user is pay or cancel or something else.
    throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $order->id(),
    ])->toString());

  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    throw new AccessDeniedHttpException();
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    throw new AccessDeniedHttpException();
  }

  /**
   * Generate vb uniq order id.
   *
   * @param $oid
   *  Order id.
   *
   * @return string
   */
  private function generateIdOrder($oid) {
    return sprintf('%06s', $oid);
  }

  /**
   * Return valid url depended on mode.
   *
   * @return string
   */
  public function getUrl() {
    if ($this->configuration['mode'] == "live") {
      return VictoriaBankManager::URL_PROD;
    }
    else {
      return VictoriaBankManager::URL_TEST;
    }
  }

  /**
   * Generate form data to send.
   *
   * @param  \Drupal\commerce_order\Entity\OrderInterface  $order
   *
   * @return array
   */
  public function getFormData(OrderInterface $order) {
    $amount = $order->getTotalPrice();
    $orderId = $this->generateIdOrder($order->id());
    $amountPrice = number_format($amount->getNumber(), 0, ',', '');
    $orderItems = $order->getItems();
    $listTitle = [];
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $orderItem */
    foreach ($orderItems as $orderItem) {
      $listTitle[] = $orderItem->getTitle();
    }

    $currentTime = $this->getCurrentTime();
    $psign = $this->victoriaBankSerializer->pSignEncrypt($orderId, $currentTime, 0, $amountPrice);

    return [
      'AMOUNT' => $amountPrice,
      'CURRENCY' => $amount->getCurrencyCode(),
      'ORDER' => $orderId,
      'DESC' => implode(', ', $listTitle),
      'MERCH_NAME' => $this->configuration['merch_name'],
      'MERCH_URL' => $this->configuration['merch_url'],
      'MERCHANT' => (string) $this->configuration['merchant'],
      'TERMINAL' => (string) $this->configuration['terminal'],
      'EMAIL' => $order->getEmail(),
      "MERCH_ADDRESS" => $this->configuration['merch_address'],
      'TRTYPE' => 0,
      'COUNTRY' => "md",
      "MERCH_GMT" => $this->getMerchantGmt(),
      'NONCE' => "11111111000000011111",
      "TIMESTAMP" => $currentTime,
      "P_SIGN" => $psign,
      "LANG" => $this->languageManager->getCurrentLanguage()->getId(),
      "BACKREF" => (Url::fromRoute("commerce_vb_md.back_ref", ['commerce_order' => $order->id()]))->setAbsolute(TRUE)
        ->toString(),
    ];
  }

  /**
   * Generate transaction option based on trtype.
   *
   * @param  \Drupal\commerce_payment\Entity\PaymentInterface  $payment
   * @param $trtype
   *
   * @return array
   */
  public function getTransactionOption(PaymentInterface $payment, $trtype) {
    $order = $payment->getOrder();
    $orderId = $this->generateIdOrder($order->id());
    $amount = number_format($payment->getAmount()->getNumber(), 0, ',', '');

    $victoriaBankData = $order->getData('victoria_bank', []);
    $currentTime = $this->getCurrentTime();
    $psign = $this->victoriaBankSerializer->pSignEncrypt($orderId, $currentTime, $trtype, $amount);
    return [
      'ORDER' => $orderId,
      'AMOUNT' => $amount,
      'CURRENCY' => $payment->getAmount()->getCurrencyCode(),
      'RRN' => $payment->getRemoteId(),
      'INT_REF' => $victoriaBankData["INT_REF"] ?? NULL,
      'TRTYPE' => $trtype,
      'TERMINAL' => $this->configuration['terminal'],
      'NONCE' => '11111111000000011111',
      'TIMESTAMP' => $currentTime,
      'P_SIGN' => $psign,
    ];
  }

  /**
   * Merchant UTC/GMT time zone offset (e.g. –3).
   * Must be provided if merchant system is located
   * in a time zone other than the gateway server's time zone.
   *
   * @return string
   */
  protected function getMerchantGmt() {
    $timezoneOffset = (float) $this->vbTimezone->getOffset(new \DateTime()) / 3600;
    if ($timezoneOffset > 0) {
      $timezoneOffset = '+' . $timezoneOffset;
    }
    return (string) $timezoneOffset;
  }

  /**
   * Create current time on UTC zone.
   *
   * @return string
   */
  protected function getCurrentTime() {
    $date = new DrupalDateTime();
    $date->setTimezone(new \DateTimeZone("UTC"));
    return $date->format('YmdHis');
  }

  /**
   * Check if exists victoria_pub.pem.
   *
   * @return bool
   */
  private function existVictoriaPubPem() {
    $path = $this->fileSystem->realpath(VictoriaBankManager::PRIVATE_VICTORIA_PUB_PEM);
    return is_file($path);
  }

  /**
   * Check if exists key.pem.
   *
   * @return bool
   */
  private function existsKeyPem() {
    $path = $this->fileSystem->realpath(VictoriaBankManager::PRIVATE_KEY_PEM);
    return is_file($path);
  }

}
