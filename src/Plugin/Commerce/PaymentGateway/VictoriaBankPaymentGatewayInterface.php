<?php


namespace Drupal\commerce_vb_md\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\HasPaymentInstructionsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;

/**
 * Interface for VictoriaBank Payment Gateway.
 */
interface VictoriaBankPaymentGatewayInterface extends SupportsVoidsInterface, HasPaymentInstructionsInterface, OffsitePaymentGatewayInterface {
  
  /**
   * Creates a payment.
   *
   * @param  \Drupal\commerce_payment\Entity\PaymentInterface  $payment
   *   The payment.
   * @param  bool  $received
   *   Whether the payment was already received.
   */
  public function createPayment(PaymentInterface $payment, $received = FALSE);
  
  
  /**
   * Receives the given payment.
   *
   * @param  \Drupal\commerce_payment\Entity\PaymentInterface  $payment
   *   The payment.
   * @param  \Drupal\commerce_price\Price  $amount
   *   The received amount. If NULL, defaults to the entire payment amount.
   */
  public function authorizationPayment(PaymentInterface $payment);
  
  /**
   * Receives the given payment.
   *
   * @param  \Drupal\commerce_payment\Entity\PaymentInterface  $payment
   *   The payment.
   */
  public function refundPayment(PaymentInterface $payment);
  
  /**
   * Receives the given payment.
   *
   * @param  \Drupal\commerce_payment\Entity\PaymentInterface  $payment
   *   The payment.
   * @param  \Drupal\commerce_price\Price  $amount
   *   The received amount. If NULL, defaults to the entire payment amount.
   */
  public function completePayment(PaymentInterface $payment);
  
  
}
