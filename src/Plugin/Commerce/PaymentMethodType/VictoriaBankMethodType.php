<?php

namespace Drupal\commerce_vb_md\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the VictoriaBank payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "victoria_bank",
 *   label = @Translation("Victoria Bank"),
 *   create_label = @Translation("New Victoria Bank"),
 * )
 */
class VictoriaBankMethodType extends PaymentMethodTypeBase {
  
  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return 'Victoria Bank';
  }
  
}
