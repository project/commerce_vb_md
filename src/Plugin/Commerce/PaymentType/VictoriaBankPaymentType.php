<?php

namespace Drupal\commerce_vb_md\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the Victoria Bank payment type.
 *
 * @CommercePaymentType(
 *   id = "victoria_bank",
 *   label = @Translation("Victoria Bank"),
 *   workflow = "victoria_bank",
 * )
 */
class VictoriaBankPaymentType extends PaymentTypeBase {
  
  /**
   * @inheritDoc
   */
  public function buildFieldDefinitions() {
    return [];
  }
  
}
