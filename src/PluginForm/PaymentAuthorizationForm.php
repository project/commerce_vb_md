<?php

namespace Drupal\commerce_vb_md\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Payment Authorization Form Vb.
 */
class PaymentAuthorizationForm extends PaymentGatewayFormBase {
  
  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }
  
  /**
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // TODO: NOTHING Client Must Pay via card for authorization
  }
  
}
