<?php

namespace Drupal\commerce_vb_md\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\commerce_vb_md\VictoriaBankManager;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Payment Complete Form Vb.
 */
class PaymentCompleteForm extends PaymentGatewayFormBase implements ContainerInjectionInterface {
  
  /**
   * Vb manager.
   *
   * @var \Drupal\commerce_vb_md\VictoriaBankManager
   */
  protected VictoriaBankManager $victoriaBankManager;
  
  /**
   * Messenger handler.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;
  
  public function __construct(VictoriaBankManager $victoriaBankManager, MessengerInterface $messenger) {
    $this->victoriaBankManager = $victoriaBankManager;
    $this->messenger = $messenger;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_vb_md.manager'),
      $container->get('messenger')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_vb_md\Plugin\Commerce\PaymentGateway\VictoriaBankPaymentGateway $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $resultOutput = $this->victoriaBankManager->referencedTransactionClientExecute($payment_gateway_plugin->getUrl(), $payment_gateway_plugin->getTransactionOption($payment, 21));
    $resultOutput = XSS::filter($resultOutput, ["table", "td", "tr", "tbody", "thead", "th", "p"]);
    $rendered_message = Markup::create($resultOutput);
    $this->messenger->addMessage($rendered_message);
    //Add some delay, victoria bank trigger callback.
    sleep(1);
  }
  
}
