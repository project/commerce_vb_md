<?php

namespace Drupal\commerce_vb_md\PluginForm;

use \Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the class for payment off-site forms.
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm {
  
  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['#parents'] = ['payment_process'];
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    
    /** @var \Drupal\commerce_vb_md\Plugin\Commerce\PaymentGateway\VictoriaBankPaymentGateway $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $data = [];
    
    foreach ($payment_gateway_plugin->getFormData($payment->getOrder()) as $k => $v) {
      $data[$k] = $v;
    }
    
    $form['#process'][] = [get_class($this), 'processVbRedirectForm'];
    $form = $this->buildRedirectForm($form, $form_state, $payment_gateway_plugin->getUrl(), $data, PaymentOffsiteForm::REDIRECT_POST);
    return $form;
  }
  
  /**
   * Unsed variable that broken send request to Vb
   *
   * @param  array  $form
   *   The plugin form.
   * @param  \Drupal\Core\Form\FormStateInterface  $form_state
   *   The current state of the form.
   * @param  array  $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed form element.
   */
  public static function processVbRedirectForm(array $form, FormStateInterface $form_state, array &$complete_form) {
    unset($complete_form['form_token']);
    unset($complete_form['form_build_id']);
    unset($complete_form['form_id']);
    $complete_form['actions']['next']['#name'] = '';
    return $form;
  }
  
}
