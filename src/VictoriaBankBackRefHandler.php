<?php

namespace Drupal\commerce_vb_md;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;

/**
 * Back ref after succes or failed on vb payment.
 */
class VictoriaBankBackRefHandler {
  
  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;
  
  /**
   * @var \Drupal\commerce_vb_md\VictoriaBankManager
   */
  protected VictoriaBankManager $victoriaBankManager;
  
  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;
  
  /**
   * @param  \Drupal\Core\Entity\EntityTypeManagerInterface  $entityTypeManager
   * @param  \Drupal\commerce_vb_md\VictoriaBankManager  $victoriaBankManager
   * @param  \Drupal\Core\Session\AccountProxyInterface  $account
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, VictoriaBankManager $victoriaBankManager, AccountProxyInterface $account) {
    $this->entityTypeManager = $entityTypeManager;
    $this->victoriaBankManager = $victoriaBankManager;
    $this->currentUser = $account;
  }
  
  /**
   * Redirect to url after back ref.
   *
   * @param  \Drupal\commerce_order\Entity\OrderInterface  $order
   *
   * @return \Drupal\Core\Url
   */
  public function redirect(OrderInterface $order): Url {
    $currentUser = \Drupal::currentUser();
    $frontUrl = Url::fromRoute("<front>");
    $access = $order->access('view', $currentUser);
    
    if (!$access) {
      return $frontUrl;
    }
    if (!$order->isLocked()) {
      sleep(1);
    }
    //Back url is access if user fail or success payment.
    //It must always back to payment page or next checkout step id.
    return Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $order->id(),
    ]);
  }
  
}
