<?php

namespace Drupal\commerce_vb_md;

use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentStorageInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_vb_md\Exception\VictoriaBankException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Vb handler.
 */
class VictoriaBankManager {

  use StringTranslationTrait;

  const URL_TEST = "https://ecomt.victoriabank.md/cgi-bin/cgi_link";

  const URL_PROD = "https://vb059.vb.md/cgi-bin/cgi_link";

  const PRIVATE_VICTORIA_PUB_PEM = "private://vicb_pem/victoria_pub.pem";

  const PRIVATE_KEY_PEM = "private://vicb_pem/key.pem";

  const FILL_OPTIONS = [
    "P_SIGN",
    "ORDER",
    "TRTYPE",
    "AMOUNT",
    "APPROVAL",
    "CURRENCY",
    "ACTION",
    "CARD",
    "INT_REF",
    "RC",
    "RRN",
    "BIN",
    "TIMESTAMP",
  ];

  /**
   * pSign Serializer.
   *
   * @var \Drupal\commerce_vb_md\VictoriaBankSerializer
   */
  protected VictoriaBankSerializer $victoriaBankSerializer;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $loggerChannelFactory;

  /**
   * EntityManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Locker.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * @param  \Drupal\commerce_vb_md\VictoriaBankSerializer  $victoriaBankSerializer
   *    pSign Serializer.
   * @param  \Drupal\Core\Entity\EntityTypeManagerInterface  $entityTypeManager
   *    EntityTypeManager
   * @param  \Drupal\Core\Logger\LoggerChannelFactoryInterface  $loggerChannelFactory
   *    Core logger.
   * @param  \Symfony\Component\EventDispatcher\EventDispatcherInterface  $event_dispatcher
   *    EventListener.
   * @param  \Drupal\Core\Lock\LockBackendInterface  $lock
   *    Locker.
   */
  public function __construct(VictoriaBankSerializer $victoriaBankSerializer, EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $loggerChannelFactory, EventDispatcherInterface $event_dispatcher, LockBackendInterface $lock) {
    $this->victoriaBankSerializer = $victoriaBankSerializer;
    $this->entityTypeManager = $entityTypeManager;
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->eventDispatcher = $event_dispatcher;
    $this->lock = $lock;
  }

  /**
   * @param $response
   *
   * @throws \Exception
   */
  public function preprocessResponse(Request $request) {
    $postData = $request->request;
    $options = [];
    foreach (self::FILL_OPTIONS as $option) {
      $options[$option] = $postData->get($option);
    }
    if (is_null($options['P_SIGN']) && is_null($options['ACTION']) && is_null($options['RC']) && is_null($options['RRN']) && is_null($options['ORDER']) && is_null($options['AMOUNT'])) {
      return;
    }
    if (!in_array($options['ACTION'], [0, 1])) {
      return;
    }
    try {
      $status = $this->victoriaBankSerializer->pSignDecrypt($options['P_SIGN'], $options['ACTION'], $options['RC'], $options['RRN'], $options['ORDER'], $options['AMOUNT']);
      if ($status === FALSE) {
        $this->getLogger()
          ->error(sprintf("Invalid psign. OrderId: %s, Actions: %s, Rc: %s, RRN: %s", $options['ORDER'], $options['ACTION'], $options['RC'], $options['RRN']));
        return;
      }
    } catch (\Exception $e) {
      $this->getLogger()->error($e->getMessage());
      return;
    }
    $order = $this->getOrderStorage()->load((int) $options['ORDER']);
    if (!$order) {
      throw new VictoriaBankException("Order not found");
    }
    $lockId = sprintf("%s_%s_%s", $order->id(), $options['TRTYPE'], $options['ACTION']);
    $isUnLock = $this->lock->acquire($lockId, 5);
    if (!$isUnLock) {
      return;
    }
    $this->generateLog($order, $options);
    $options['ORDER'] = $order;
    switch ($options['TRTYPE']) {
      case 0 :
        $this->preprocessAuthorization($options);
        break;
      case 21:
        $this->preprocessCompleteSales($options);
        break;
      case 24:
        $this->preprocessRefund($options);
        break;
    }
    $this->lock->release($lockId);

  }

  /**
   * Send reference execution
   *
   * @param $url
   * @param $options
   *
   * @return bool|string
   */
  public function referencedTransactionClientExecute($url, $options) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    $listOpt = [];
    foreach ($options as $k => $v) {
      $listOpt[] = $k . "=" . $v;
    }
    $postFields = implode("&", $listOpt);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $server_output = curl_exec($ch);
    curl_close($ch);
    return $server_output;
  }

  private function preprocessAuthorization(array $options) {
    /** @var OrderInterface $order */
    $order = $options['ORDER'];

    //If exist already payment, do nothing.
    $payment = $this->findPaymentInOrder($order);
    if ($payment) {
      return;
    }

    $price = new Price($options['AMOUNT'], 'MDL');
    $payment = $this->createNewPayment($order, $price, $options['RRN']);
    $payment->getPaymentGateway()
      ->getPlugin()
      ->authorizationPayment($payment);

    $order->setData("victoria_bank", $options);
    $order->save();
    //Here must move order to the next step.
    /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkoutFlow */
    $checkoutFlow = $order->get('checkout_flow')->entity;

    $checkoutFlowPlugin = $checkoutFlow->getPlugin();

    //Always here next step, because order is locked.
    $nextStepId = $checkoutFlowPlugin->getNextStepId("payment");
    $order->set('checkout_step', $nextStepId);
    //This method is private. We can't used, but handler manualy
    //$order->onStepChange($step_id);
    if ($nextStepId == 'payment') {
      $order->lock();
    }
    elseif ($nextStepId != 'payment') {
      $order->unlock();
    }
    // Place the order.
    if ($nextStepId == 'complete' && $order->getState()->getId() == 'draft') {
      // Notify other modules.
      $event = new OrderEvent($order);
      $this->eventDispatcher->dispatch($event, CheckoutEvents::COMPLETION);
      $order->getState()->applyTransitionById('place');
    }
    $order->save();
    /** @var \Drupal\commerce_vb_md\Plugin\Commerce\PaymentGateway\VictoriaBankPaymentGateway $paymentGatewayPlugin */
    $paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();
    //Before send mail need to check if auto_complete_sale is active and not send mail information 00.
    $autoCompleteSales = $paymentGatewayPlugin->getConfiguration()['auto_complete_sales'] == 1 ?? FALSE;

    if ($autoCompleteSales) {
      $this->referencedTransactionClientExecute($paymentGatewayPlugin->getUrl(), $paymentGatewayPlugin->getTransactionOption($payment, 21));
    } else {
      $this->sendOrderMailClientInformation($order, "00");
    }
  }

  /**
   * Create payment.
   *
   * @param  \Drupal\commerce_order\Entity\OrderInterface  $order
   * @param  \Drupal\commerce_price\Price  $price
   * @param $rrn
   *
   * @return \Drupal\commerce_payment\Entity\Payment
   */
  private function createNewPayment(OrderInterface $order, Price $price, $rrn) {
    return Payment::create([
      'order_id' => $order->id(),
      'payment_gateway' => 'victoria_bank',
      'amount' => $price,
      'state' => 'new',
      'remote_id' => $rrn,
    ]);
  }

  /**
   * Complete flow for payment.
   *
   * @param  array  $options
   *
   * @return void
   */
  private function preprocessCompleteSales(array $options) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $options['ORDER'];
    $payment = $this->findPaymentInOrder($order);
    /** @var \Drupal\commerce_vb_md\Plugin\Commerce\PaymentGateway\VictoriaBankPaymentGateway $paymentGatewayPlugin */
    $paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();
    $paymentGatewayPlugin->completePayment($payment);
    $this->sendOrderMailClientInformation($order, 21);
  }

  /**
   * Refund flow for payment.
   *
   * @param  array  $options
   *
   * @return void
   */
  private function preprocessRefund(array $options) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $options['ORDER'];
    $payment = $this->findPaymentInOrder($order);
    /** @var \Drupal\commerce_vb_md\Plugin\Commerce\PaymentGateway\VictoriaBankPaymentGateway $paymentGatewayPlugin */
    $paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();
    $paymentGatewayPlugin->refundPayment($payment);
    $this->sendOrderMailClientInformation($order, 24);
  }

  public function sendOrderMailClientInformation(Order $order, $trType) {

    switch ($trType) {
      case 21:
        $subjectSuffix = t('Successful payment');
        break;
      case 24:
        $subjectSuffix = t("Payment returned");
        break;
      default:
        $subjectSuffix = t("Authorized payment");
    }

    $theme = [
      '#theme' => 'commerce_vb_md_mail',
      '#order' => $order,
      '#trtype' => $trType,
    ];
    $renderedMail = \Drupal::service('renderer')->render($theme);
    $params['body'] = $renderedMail;
    $params['subject'] = t("Order: @order_id - @payment_status", [
      '@order_id' => $order->getOrderNumber(),
      '@payment_status' => $subjectSuffix,
    ]);
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'commerce_vb_md';
    $key = 'transaction_status';
    $to = $order->getEmail();
    $langcode = \Drupal::languageManager()->getDefaultLanguage()->getId();
    $mailManager->mail($module, $key, $to, $langcode, $params, NULL, TRUE);
  }

  /**
   * Search existed payment victoria bank payment in order.
   *
   * @return void
   */
  public function findPaymentInOrder(OrderInterface $order): ?PaymentInterface {
    $paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
    $payments = $paymentStorage->loadMultipleByOrder($order);
    $paymentVictoriaBank = NULL;
    /** @var Payment $payment */
    foreach ($payments as $payment) {
      if ($payment->getPaymentGatewayId() == 'victoria_bank') {
        $paymentVictoriaBank = $payment;
      }
    }
    return $paymentVictoriaBank;
  }

  private function generateLog(OrderInterface $order, array $options) {
    $lists = [];
    unset($options['P_SIGN']);
    foreach ($options as $key => $item) {
      $lists[] = sprintf("%s:%s", $key, $item);
    }
    /** @var \Drupal\commerce_log\LogStorageInterface $logStorage */
    $logStorage = $this->entityTypeManager->getStorage('commerce_log');
    $logStorage->generate($order, 'vb_md_callback', ['lists' => $lists])
      ->save();
  }

  /**
   * @return \Drupal\commerce_order\OrderStorage
   */
  public function getOrderStorage() {
    return $this->entityTypeManager->getStorage('commerce_order');
  }

  /**
   * @return PaymentStorageInterface;
   */
  protected function getPaymentStorage() {
    return $this->entityTypeManager->getStorage('commerce_payment');
  }

  /**
   * @return \Drupal\Core\Logger\LoggerChannel|\Drupal\Core\Logger\LoggerChannelInterface
   */
  public function getLogger() {
    return $this->loggerChannelFactory->get('commerce_vb_md');
  }

}
