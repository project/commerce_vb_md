<?php

namespace Drupal\commerce_vb_md;

use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;

class VictoriaBankSerializer {
  
  private FileSystemInterface $fileSystem;
  
  public function __construct(FileSystem $fileSystem) {
    $this->fileSystem = $fileSystem;
  }
  
  
  /**
   * pSign Decrypt.
   *
   * @param $P_SIGN
   * @param $ACTION
   * @param $RC
   * @param $RRN
   * @param $ORDER
   * @param $AMOUNT
   *
   * @return bool
   * @throws \Exception
   */
  public function pSignDecrypt($P_SIGN, $ACTION, $RC, $RRN, $ORDER, $AMOUNT) {
    $InData = [
      'ACTION' => $ACTION,
      'RC' => $RC,
      'RRN' => $RRN,
      'ORDER' => $ORDER,
      'AMOUNT' => $AMOUNT,
    ];
    $MAC = '';
    foreach ($InData as $Id => $Filed) {
      if ($Filed != '-') : $MAC .= strlen($Filed) . $Filed;
      else: $MAC .= $Filed; endif;
    }
    $MD5_Hash_In = strtoupper(md5($MAC));
    $P_SIGNBIN = hex2bin($P_SIGN);
    $privateKeyPem = VictoriaBankManager::PRIVATE_VICTORIA_PUB_PEM;
    $RSA_KeyPath = $this->fileSystem->realpath($privateKeyPem);
    $RSA_Key = file_get_contents($RSA_KeyPath);
    if (!$RSA_KeyResource = openssl_get_publickey($RSA_Key)) {
      throw new \Exception('Failed get public key');
    }
    
    
    if (!openssl_public_decrypt($P_SIGNBIN, $DECRYPTED_BIN, $RSA_Key)) {
      throw new \Exception('Failed decrypt');
      //          while ($msg = openssl_error_string()) echo $msg . "<br />\n";
      //          die ('Failed decrypt');
    }
    
    $DECRYPTED = strtoupper(bin2hex($DECRYPTED_BIN));
    $Prefix = '3020300C06082A864886F70D020505000410';
    $DECRYPTED_HASH = str_replace($Prefix, '', $DECRYPTED);
    
    if ($DECRYPTED_HASH == $MD5_Hash_In) {
      $RESULT = TRUE;
    }
    else {
      $RESULT = FALSE;
    }
    
    return $RESULT;
    
  }
  
  /**
   * pSign encrypts.
   *
   * @param $OrderId
   * @param $Timestamp
   * @param $trtType
   * @param $Amount
   *
   * @return string|void
   */
  function pSignEncrypt($OrderId, $Timestamp, $trtType, $Amount) {
    $MAC = '';
    $privateKeyPem = VictoriaBankManager::PRIVATE_KEY_PEM;
    $RSA_KeyPath = $this->fileSystem->realpath($privateKeyPem);
    $RSA_Key = file_get_contents($RSA_KeyPath);
    $Data = [
      'ORDER' => $OrderId,
      'NONCE' => '11111111000000011111',
      'TIMESTAMP' => $Timestamp,
      'TRTYPE' => $trtType,
      'AMOUNT' => $Amount,
    ];
    
    if (!$RSA_KeyResource = openssl_get_privatekey($RSA_Key)) {
      die ('Failed get private key');
    }
    $RSA_KeyDetails = openssl_pkey_get_details($RSA_KeyResource);
    $RSA_KeyLength = $RSA_KeyDetails['bits'] / 8;
    
    foreach ($Data as $Id => $Filed) {
      $MAC .= strlen($Filed) . $Filed;
    }
    
    $First = '0001';
    $Prefix = '003020300C06082A864886F70D020505000410';
    $MD5_Hash = md5($MAC);
    $Data = $First;
    
    $paddingLength = $RSA_KeyLength - strlen($MD5_Hash) / 2 - strlen($Prefix) / 2 - strlen($First) / 2;
    for ($i = 0; $i < $paddingLength; $i++) {
      $Data .= "FF";
    }
    
    $Data .= $Prefix . $MD5_Hash;
    $BIN = pack("H*", $Data);
    
    if (!openssl_private_encrypt($BIN, $EncryptedBIN, $RSA_Key, OPENSSL_NO_PADDING)) {
      while ($msg = openssl_error_string()) {
        echo $msg . "<br />\n";
      }
      die ('Failed encrypt');
    }
    
    $P_SIGN = bin2hex($EncryptedBIN);
    
    return strtoupper($P_SIGN);
  }
  
}
